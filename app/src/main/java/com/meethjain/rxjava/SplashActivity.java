package com.meethjain.rxjava;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SplashActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_splash);
    ButterKnife.bind(this);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
    fab.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
            .setAction("Action", null)
            .show();
      }
    });
  }

  @OnClick({R.id.btn_sign_in, R.id.btn_sign_in_new, R.id.btn_movie, R.id.btn_flowable})
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.btn_sign_in: {
        startActivity(RxActivity.getMyIntent(this, Constants.TYPE_SIGN_IN));
        break;
      }
      case R.id.btn_sign_in_new: {
        startActivity(RxActivity.getMyIntent(this, Constants.TYPE_SIGN_IN_NEW));
        break;
      }
      case R.id.btn_movie: {
        startActivity(RxActivity.getMyIntent(this, Constants.TYPE_MOVIE));
        break;
      }
      case R.id.btn_flowable: {
        startActivity(RxActivity.getMyIntent(this, Constants.TYPE_FLOWABLE));
        break;
      }
    }
  }
}
