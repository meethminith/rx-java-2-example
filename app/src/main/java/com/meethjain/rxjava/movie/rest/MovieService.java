package com.meethjain.rxjava.movie.rest;

import com.meethjain.rxjava.movie.Movie;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created on 31/05/17.
 *
 * @author Meeth D Jain
 */

public interface MovieService {

  //String MOVIE_API = "v2/592fbb91110000c40fb3923f";
  String MOVIE_API = "movies.json";

  @GET("movies.json")
  Call<List<Movie>> getMovies();
}
