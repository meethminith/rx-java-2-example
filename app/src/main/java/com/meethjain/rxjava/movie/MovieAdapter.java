package com.meethjain.rxjava.movie;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.meethjain.rxjava.R;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 31/05/17.
 *
 * @author Meeth D Jain
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieHolder> {

  private Picasso mPicasso;
  private List<Movie> mMovieLists;

  public MovieAdapter(Picasso picasso) {
    mMovieLists = new ArrayList<>();
    mPicasso = picasso;
  }

  @Override
  public int getItemCount() {
    return mMovieLists.size();
  }

  @Override
  public void onBindViewHolder(MovieHolder holder, int position) {
    holder.titleTv.setText(mMovieLists.get(position).getTitle());
    holder.ratingTv.setText(String.valueOf(mMovieLists.get(position).getRating()));
    holder.releaseTv.setText(String.valueOf(mMovieLists.get(position).getReleaseYear()));
    String genere = "";
    for(String s: mMovieLists.get(position).getGenre()){
      genere += " " +s;
    }
    holder.genereTv.setText(genere);
    mPicasso.load(mMovieLists.get(position).getImage()).into(holder.imageView);
  }

  @Override
  public MovieHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
    return new MovieHolder(view);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  public void add(List<Movie> movieLists) {
    mMovieLists.clear();
    mMovieLists.addAll(movieLists);
    notifyDataSetChanged();
  }

  static class MovieHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.title_tv)
    TextView titleTv;
    @BindView(R.id.rating_tv)
    TextView ratingTv;
    @BindView(R.id.releaseyear_tv)
    TextView releaseTv;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.genere_tv)
    TextView genereTv;

    MovieHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }
  }
}
