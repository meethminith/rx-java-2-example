package com.meethjain.rxjava;

/**
 * Created on 01/06/17.
 *
 * @author Meeth D Jain
 */

public interface Constants {
  int TYPE_SIGN_IN = 0;
  int TYPE_SIGN_IN_NEW = 1;
  int TYPE_MOVIE = 2;
  int TYPE_FLOWABLE = 3;
}
