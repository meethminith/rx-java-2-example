package com.meethjain.rxjava.signin;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.ProgressBar;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created on 31/05/17.
 *
 * @author Meeth D Jain
 */

public class BaseFragment extends Fragment {

  private ProgressDialog mProgressDialog;
  private CompositeDisposable mCompositeDisposable;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    mCompositeDisposable = new CompositeDisposable();
    mProgressDialog = new ProgressDialog(getActivity());
    mProgressDialog.setCancelable(true);
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    dispose();
  }

  protected void add(Disposable disposable) {
    if (mCompositeDisposable != null) {
      mCompositeDisposable.add(disposable);
    }
  }

  protected void dispose() {
    if (mCompositeDisposable != null && !mCompositeDisposable.isDisposed()) {
      mCompositeDisposable.dispose();
    }
  }

  protected void showDialog(String title, String message) {
    dismissDialog();
    mProgressDialog.setTitle(title);
    mProgressDialog.setMessage(message);
    mProgressDialog.show();
  }

  protected void dismissDialog() {
    if (mProgressDialog != null && mProgressDialog.isShowing()) {
      mProgressDialog.dismiss();
    }
  }
}
