package com.meethjain.rxjava.utils;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.widget.Toast;

/**
 * Created on 01/06/17.
 *
 * @author Meeth D Jain
 */

public class UiUtils {

  public static void showToast(Context context, String message) {
    if (context != null && !TextUtils.isEmpty(message)) {
      Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
  }

  public static void showSnackbar(Activity activity, String message) {
    Snackbar.make(activity.findViewById(android.R.id.content),
        message,
        Snackbar.LENGTH_LONG)
        .show();
  }
}
