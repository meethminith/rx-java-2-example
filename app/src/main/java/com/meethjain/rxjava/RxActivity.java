package com.meethjain.rxjava;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import butterknife.ButterKnife;
import com.meethjain.rxjava.movie.MovieFragment;
import com.meethjain.rxjava.signin.SignInFragment;
import com.meethjain.rxjava.signin.SignInNewFragment;
import com.meethjain.rxjava.utils.FlowableFragment;

/**
 * Created on 31/05/17.
 *
 * @author Meeth D Jain
 */

public class RxActivity extends AppCompatActivity {

  public static Intent getMyIntent(Context context, int exampleType) {
    Intent intent = new Intent(context, RxActivity.class);
    intent.putExtra("example_type", exampleType);
    return intent;
  }

  // region Lifecycle Methods
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_rx);
    ButterKnife.bind(this);

    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);
    if (fragment == null) {
      fragment = getFragment(getIntent().getIntExtra("example_type", 0));
      getSupportFragmentManager()
          .beginTransaction()
          .replace(android.R.id.content, fragment, "")
          .commit();
    } else {
      getSupportFragmentManager()
          .beginTransaction()
          .attach(fragment)
          .commit();
    }
  }

  public static Fragment getFragment(int exampleType) {
    switch (exampleType) {
      case Constants.TYPE_SIGN_IN: return SignInFragment.newInstance();
      case Constants.TYPE_SIGN_IN_NEW: return SignInNewFragment.newInstance();
      case Constants.TYPE_MOVIE: return MovieFragment.newInstance();
      case Constants.TYPE_FLOWABLE: return FlowableFragment.newInstance();
      default: return SignInNewFragment.newInstance();
    }
  }
}