package com.meethjain.rxjava.movie;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.meethjain.rxjava.R;
import com.meethjain.rxjava.movie.rest.RestClient;
import com.meethjain.rxjava.signin.BaseFragment;
import com.meethjain.rxjava.utils.LogUtil;
import com.meethjain.rxjava.utils.UiUtils;
import com.squareup.picasso.Picasso;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.MaybeObserver;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.internal.operators.maybe.MaybeObserveOn;
import io.reactivex.schedulers.Schedulers;
import java.util.List;
import java.util.concurrent.Callable;
import retrofit2.Response;

public class MovieFragment extends BaseFragment {

  @BindView(R.id.rv_movie)
  RecyclerView mMovieRecyclerView;

  private MovieAdapter mMovieAdapter;

  private SingleObserver<List<Movie>> mMovieObserver = new SingleObserver<List<Movie>>() {
    @Override
    public void onSubscribe(@NonNull Disposable d) {
      LogUtil.d("SingleObserver: onSubscribe");
      add(d);
    }

    @Override
    public void onSuccess(@NonNull List<Movie> movies) {
      LogUtil.d("SingleObserver: onSuccess");
      dismissDialog();
      UiUtils.showToast(getActivity(), "Success");
      onMoviesLoaded(movies);
    }

    @Override
    public void onError(@NonNull Throwable e) {
      LogUtil.d("SingleObserver : onError");
      dismissDialog();
      UiUtils.showToast(getActivity(), "Error : " + e.getMessage());
    }
  };

  private MaybeObserver<List<Movie>> mMovieMayberObserver = new MaybeObserver<List<Movie>>() {
    @Override
    public void onSubscribe(@NonNull Disposable d) {
      LogUtil.d("MaybeObserver: onSubscribe");
      add(d);
    }

    @Override
    public void onSuccess(@NonNull List<Movie> movies) {
      LogUtil.d("MaybeObserver: onSuccess");
      dismissDialog();
      UiUtils.showToast(getActivity(), "Success");
      onMoviesLoaded(movies);
    }

    @Override
    public void onError(@NonNull Throwable e) {
      LogUtil.d("MaybeObserver : onError");
      dismissDialog();
      UiUtils.showToast(getActivity(), "Error : " + e.getMessage());
    }

    @Override
    public void onComplete() {
      LogUtil.d("MaybeObserver : onComplete");
      dismissDialog();
      UiUtils.showToast(getActivity(), "Complete : ");
    }
  };

  private Single<Response<List<Movie>>> mSingle = Single.fromCallable(new Callable<Response<List<Movie>>>() {
    @Override
    public Response<List<Movie>> call() throws Exception {
      //Thread.sleep(5000);
      LogUtil.d("Single : call");
      Response<List<Movie>> response = RestClient.getMovieService().getMovies().execute();
      return response;
    }
  });

  private Completable mCompletable = Completable.fromRunnable(new Runnable() {
    @Override
    public void run() {
      LogUtil.d("Completable : run");
      uploadToServer();
    }
  });

  public static MovieFragment newInstance() {
    return new MovieFragment();
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    getActivity().setTitle("Movies");
  }

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_movie, container, false);
    ButterKnife.bind(this, view);
    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
    mMovieAdapter = new MovieAdapter(Picasso.with(getActivity()));
    mMovieRecyclerView.setAdapter(mMovieAdapter);
    mMovieRecyclerView.setLayoutManager(layoutManager);

    return view;
  }

  @OnClick({R.id.btn_load_movie, R.id.fab_upload})
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.btn_load_movie: {
        fetchMovie();
        break;
      }
      case R.id.fab_upload: {
        initiateServerUpload();
        //initiateServerUploadV2();
        break;
      }
    }
  }

  private void fetchMovie() {
    mSingle
        .subscribeOn(Schedulers.io())
        .map(new Function<Response<List<Movie>>, List<Movie>>() {
          @Override
          public List<Movie> apply(@NonNull Response<List<Movie>> listCall) throws Exception {
            LogUtil.d("Single : map");
            //Thread.sleep(15000);
            return listCall.body();
          }
        })
        .observeOn(AndroidSchedulers.mainThread())
        .doOnSubscribe(new Consumer<Disposable>() {
          @Override
          public void accept(@NonNull Disposable disposable) throws Exception {
            LogUtil.d("Single : doOnSubscribe");
            showDialog("Loading Movies", "Please wait while we load");
          }
        })
        .subscribe(mMovieObserver);
  }

  private void onMoviesLoaded(List<Movie> movieList) {
    mMovieAdapter.add(movieList);
  }

  private void initiateServerUpload() {
    Disposable disposable = mCompletable
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .doOnSubscribe(new Consumer<Disposable>() {
          @Override
          public void accept(@NonNull Disposable disposable) throws Exception {
            LogUtil.d("Completable : doOnSubscribe");
            showDialog("Uploading", "Please wait while we upload");
          }
        })
        .subscribe(new Action() {
          @Override public void run() throws Exception {
            LogUtil.d("Completable : onSuccess");
            dismissDialog();
            UiUtils.showSnackbar(getActivity(), "Uploaded");
          }
        }, new Consumer<Throwable>() {
          @Override
          public void accept(@NonNull Throwable throwable) throws Exception {
            LogUtil.d("Completable : onError");
            dismissDialog();
            UiUtils.showSnackbar(getActivity(), "Upload Failed");
          }
        });
    add(disposable);
  }

  //private Completable mCompletableLambda = Completable.fromCallable(new Callable<Void>() {
  //  @Override
  //  public Void call() throws Exception {
  //    uploadToServer();
  //    throw new Exception();
  //  }
  //});
  ////private Completable mCompletableLambda = Completable.fromRunnable(this::uploadToServer);
  //
  //private void initiateServerUploadV2() {
  //  Disposable diposable = mCompletableLambda
  //      .subscribeOn(Schedulers.newThread())
  //      .doOnSubscribe((d) -> showDialog("Uploading v2", "Please wait while we upload"))
  //      .observeOn(AndroidSchedulers.mainThread())
  //      .subscribe(() -> {
  //            dismissDialog();
  //            UiUtils.showSnackbar(getActivity(), "Uploaded");
  //          },
  //          e -> {
  //            dismissDialog();
  //            UiUtils.showSnackbar(getActivity(), "Upload Failed");
  //          });
  //  add(diposable);
  //}

  private void uploadToServer() {
    try {
      Thread.sleep(5000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
