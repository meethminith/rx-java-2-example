package com.meethjain.rxjava.utils;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import butterknife.BindView;
import com.jakewharton.rxbinding2.view.RxView;
import com.meethjain.rxjava.R;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;

/**
 * Created on 01/06/17.
 *
 * @author Meeth D Jain
 */

public class FlowableFragment extends Fragment {

  @BindView(R.id.email_til)
  TextInputLayout emailInputLayout;
  @BindView(R.id.password_til)
  TextInputLayout passwordInputLayout;
  @BindView(R.id.password_et)
  EditText passwordEditText;
  @BindView(R.id.email_et)
  EditText emailEditText;
  @BindView(R.id.sign_in_ll)
  LinearLayout signInLinearLayout;
  @BindView(R.id.sign_in_btn)
  Button signInButton;

  public static FlowableFragment newInstance() {
    return new FlowableFragment();
  }

  private Flowable<DragEvent> mFlowable = RxView.drags(null).toFlowable(BackpressureStrategy.BUFFER);

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {

    return super.onCreateView(inflater, container, savedInstanceState);
  }
}
