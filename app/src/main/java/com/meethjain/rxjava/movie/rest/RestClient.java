package com.meethjain.rxjava.movie.rest;

import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created on 31/05/17.
 *
 * @author Meeth D Jain
 */

public class RestClient {

  private static final String BASE_URL = "http://api.androidhive.info/json/";
  //private static final String BASE_URL = "http://www.mocky.io/";
  private static Retrofit retrofit = null;

  private static Retrofit getRetrofit() {
    if (retrofit == null) {
      OkHttpClient okHttpClient = new OkHttpClient.Builder()
          .connectTimeout(10, TimeUnit.SECONDS)
          .readTimeout(10, TimeUnit.SECONDS)
          .build();

      retrofit = new Retrofit.Builder()
          .baseUrl("http://api.androidhive.info/json/")
          .client(okHttpClient)
          .addConverterFactory(GsonConverterFactory.create())
          .build();
    }
    return retrofit;
  }

  public static MovieService getMovieService() {
    return getRetrofit().create(MovieService.class);
  }
}
