package com.meethjain.rxjava.signin;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.meethjain.rxjava.R;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created on 31/05/17.
 *
 * @author Meeth D Jain
 */

public class SignInNewFragment extends BaseFragment {

  @BindView(R.id.email_til)
  TextInputLayout emailInputLayout;
  @BindView(R.id.password_til)
  TextInputLayout passwordInputLayout;
  @BindView(R.id.password_et)
  EditText passwordEditText;
  @BindView(R.id.email_et)
  EditText emailEditText;
  @BindView(R.id.sign_in_ll)
  LinearLayout signInLinearLayout;
  @BindView(R.id.sign_in_btn)
  Button signInButton;

  private Pattern pattern = android.util.Patterns.EMAIL_ADDRESS;
  private Matcher matcher;

  public static SignInNewFragment newInstance() {
    return new SignInNewFragment();
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    getActivity().setTitle("Sign In New");
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_sign_in, container, false);
    ButterKnife.bind(this, rootView);
    return rootView;
  }

  @Override
  public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    Observable<CharSequence> emailChangeObservable = RxTextView.textChanges(emailEditText);
    Observable<CharSequence> passwordChangeObservable = RxTextView.textChanges(passwordEditText);

    Disposable emailDisposable = emailChangeObservable
        .doOnNext(cs -> checkEmailError(false))
        .subscribeOn(Schedulers.io())
        .debounce(400, TimeUnit.MILLISECONDS)
        .filter(cs -> !TextUtils.isEmpty(cs))
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(cs -> checkEmailError(!validateEmail(cs.toString())),
            Throwable::printStackTrace);

    // Checks for validity of the password input field
    Disposable passwordDisposable = passwordChangeObservable
        .doOnNext(cs -> checkPasswordError(false))
        .debounce(400, TimeUnit.MILLISECONDS)
        .filter(cs -> {
          //Thread.sleep(15000);
          return !TextUtils.isEmpty(cs);
        })
        .observeOn(AndroidSchedulers.mainThread()) // UI Thread
        .subscribe(cs -> checkPasswordError(!validatePassword(cs.toString())),
            Throwable::printStackTrace);

    // Checks for validity of all input fields
    Disposable combinedDisposable = Observable.combineLatest(emailChangeObservable, passwordChangeObservable,
        (email, password) -> {
          boolean isEmailValid = validateEmail(email.toString());
          boolean isPasswordValid = validatePassword(password.toString());
          return isEmailValid && isPasswordValid;
        })
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::updateSignIn);

    add(emailDisposable);
    add(passwordDisposable);
    add(combinedDisposable);
  }

  @Override
  public void onResume() {
    super.onResume();
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
  }

  @OnClick(R.id.sign_in_btn)
  public void onSignInButtonClicked() {
    Snackbar.make(getActivity().findViewById(android.R.id.content),
        "Signed In",
        Snackbar.LENGTH_LONG)
        .show();
  }

  // region Helper Methods
  private void enableError(TextInputLayout textInputLayout) {
    if (textInputLayout.getChildCount() == 2)
      textInputLayout.getChildAt(1).setVisibility(View.VISIBLE);
  }

  private void disableError(TextInputLayout textInputLayout) {
    if (textInputLayout.getChildCount() == 2)
      textInputLayout.getChildAt(1).setVisibility(View.GONE);
  }

  private boolean validateEmail(String email) {
    if (TextUtils.isEmpty(email))
      return false;

    matcher = pattern.matcher(email);
    return matcher.matches();
  }

  private boolean validatePassword(String password) {
    return password.length() > 5;
  }

  private void checkEmailError(boolean isError) {
    if (isError) {
      enableError(emailInputLayout);
      emailInputLayout.setError("Invalid Email");
    } else {
      disableError(emailInputLayout);
      emailInputLayout.setError(null);
    }
  }

  private void checkPasswordError(boolean isError) {
    if (isError) {
      enableError(passwordInputLayout);
      passwordInputLayout.setError("Invalid Password");
    } else {
      disableError(passwordInputLayout);
      passwordInputLayout.setError(null);
    }
  }

  private void updateSignIn(boolean isEnabled) {
    if (isEnabled) {
      signInLinearLayout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
      signInButton.setEnabled(true);
      signInButton.setTextColor(ContextCompat.getColor(getContext(), android.R.color.white));
    } else {
      signInLinearLayout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
      signInButton.setEnabled(false);
      signInButton.setTextColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
    }
  }
}
