package com.meethjain.rxjava.signin;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.meethjain.rxjava.R;
import com.meethjain.rxjava.utils.LogUtil;
import com.meethjain.rxjava.utils.UiUtils;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Predicate;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created on 31/05/17.
 *
 * @author Meeth D Jain
 */

public class SignInFragment extends BaseFragment {

  @BindView(R.id.email_til)
  TextInputLayout emailInputLayout;
  @BindView(R.id.password_til)
  TextInputLayout passwordInputLayout;
  @BindView(R.id.password_et)
  EditText passwordEditText;
  @BindView(R.id.email_et)
  EditText emailEditText;
  @BindView(R.id.sign_in_ll)
  LinearLayout signInLinearLayout;
  @BindView(R.id.sign_in_btn)
  Button signInButton;

  private Pattern pattern = android.util.Patterns.EMAIL_ADDRESS;

  public static SignInFragment newInstance() {
    return new SignInFragment();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_sign_in, container, false);
    ButterKnife.bind(this, rootView);
    return rootView;
  }

  @Override
  public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    Observable<CharSequence> emailChangeObservable = RxTextView.textChanges(emailEditText);
    Observable<CharSequence> passwordChangeObservable = RxTextView.textChanges(passwordEditText);

    emailChangeObservable
        .doOnNext(new Consumer<CharSequence>() {
          @Override
          public void accept(@NonNull CharSequence charSequence) throws Exception {
            LogUtil.d("Email : doOnNext");
            hideEmailError();
          }
        })
        .debounce(400, TimeUnit.MILLISECONDS)
        .filter(new Predicate<CharSequence>() {
          @Override
          public boolean test(@NonNull CharSequence charSequence) throws Exception {
            LogUtil.d("Email : filter");
            return !TextUtils.isEmpty(charSequence);
          }
        })
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<CharSequence>() {
          @Override
          public void onSubscribe(Disposable d) {
            LogUtil.d("Email : onSubscribe");
            add(d);
          }

          @Override
          public void onNext(CharSequence charSequence) {
            LogUtil.d("Email : onNext");
            boolean isEmailValid = validateEmail(charSequence.toString());
            if (!isEmailValid) {
              showEmailError();
            } else {
              hideEmailError();
            }
          }

          @Override
          public void onError(Throwable e) {
            LogUtil.d("Email : onError");
            e.printStackTrace();
          }

          @Override
          public void onComplete() {
            LogUtil.d("Email : onComplete");
          }
        });

    // Checks for validity of the password input field
    passwordChangeObservable
        .doOnNext(new Consumer<CharSequence>() {
          @Override public void accept(@NonNull CharSequence charSequence) throws Exception {
            LogUtil.d("Password : doOnNext");
            hidePasswordError();
          }
        })
        .debounce(400, TimeUnit.MILLISECONDS)
        .filter(new Predicate<CharSequence>() {
          @Override public boolean test(@NonNull CharSequence charSequence) throws Exception {
            LogUtil.d("Password : filter");
            return !TextUtils.isEmpty(charSequence);
          }
        })
        .observeOn(AndroidSchedulers.mainThread()) // UI Thread
        .subscribe(new Observer<CharSequence>() {
          @Override
          public void onSubscribe(Disposable d) {
            LogUtil.d("Password : onSubscribe");
            add(d);
          }

          @Override
          public void onNext(CharSequence charSequence) {
            LogUtil.d("Password : onNext");
            boolean isPasswordValid = validatePassword(charSequence.toString());
            if (!isPasswordValid) {
              showPasswordError();
            } else {
              hidePasswordError();
            }
          }

          @Override
          public void onError(Throwable e) {
            LogUtil.d("Password : onError");
          }

          @Override
          public void onComplete() {
            LogUtil.d("Password : onComplete");
          }
        });

    // Checks for validity of all input fields
    Observable.combineLatest(emailChangeObservable, passwordChangeObservable,
        new BiFunction<CharSequence, CharSequence, Boolean>() {
          @Override public Boolean apply(@NonNull CharSequence charSequence,
              @NonNull CharSequence charSequence2) throws Exception {
            LogUtil.d("SignIn : combineLatest");
            boolean isEmailValid = validateEmail(charSequence.toString());
            boolean isPasswordValid = validatePassword(charSequence2.toString());
            return isEmailValid && isPasswordValid;
          }
        })
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Boolean>() {
          @Override
          public void onSubscribe(Disposable d) {
            LogUtil.d("SignIn : onSubscribe");
            add(d);
          }

          @Override
          public void onNext(Boolean aBoolean) {
            LogUtil.d("SignIn : onNext");
            updateSignIn(aBoolean);
          }

          @Override
          public void onError(Throwable e) {
            LogUtil.d("SignIn : onError");
          }

          @Override
          public void onComplete() {
            LogUtil.d("SignIn : onComplete");
          }
        });
  }

  @Override
  public void onResume() {
    super.onResume();
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
  }

  @OnClick(R.id.sign_in_btn)
  public void onSignInButtonClicked(View view) {
    UiUtils.showSnackbar(getActivity(), "Signed In");
  }

  // region Helper Methods
  private void enableError(TextInputLayout textInputLayout) {
    if (textInputLayout.getChildCount() == 2)
      textInputLayout.getChildAt(1).setVisibility(View.VISIBLE);
  }

  private void disableError(TextInputLayout textInputLayout) {
    if (textInputLayout.getChildCount() == 2)
      textInputLayout.getChildAt(1).setVisibility(View.GONE);
  }

  private boolean validateEmail(String email) {
    if (TextUtils.isEmpty(email))
      return false;

    Matcher matcher = pattern.matcher(email);
    return matcher.matches();
  }

  private boolean validatePassword(String password) {
    return password.length() > 5;
  }

  private void showEmailError(){
    enableError(emailInputLayout);
    emailInputLayout.setError("Invalid Email");
  }

  private void hideEmailError(){
    disableError(emailInputLayout);
    emailInputLayout.setError(null);
  }

  private void showPasswordError(){
    enableError(passwordInputLayout);
    passwordInputLayout.setError("Invalid Password");
  }

  private void hidePasswordError(){
    disableError(passwordInputLayout);
    passwordInputLayout.setError(null);
  }

  private void updateSignIn(boolean isEnabled) {
    if (isEnabled) {
      signInLinearLayout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
      signInButton.setEnabled(true);
      signInButton.setTextColor(ContextCompat.getColor(getContext(), android.R.color.white));
    } else {
      signInLinearLayout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
      signInButton.setEnabled(false);
      signInButton.setTextColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
    }
  }
}
