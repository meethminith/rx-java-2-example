package com.meethjain.rxjava.utils;

import android.util.Log;

/**
 * Created on 01/06/17.
 *
 * @author Meeth D Jain
 */

public class LogUtil {

  public static void d(String message) {
    Log.d("RxJavaThread", message + " Thread : " + Thread.currentThread().getName());
  }

}
